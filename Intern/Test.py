from flask import Flask, render_template, request, abort
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import RadioField, DateField, Form, BooleanField, StringField, SelectField, TextField, IntegerField, SubmitField, validators
from wtforms.validators import InputRequired, Length, DataRequired, Email, NumberRange

app = Flask(__name__)

####################################################
# Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:admin1999@localhost/Management'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'Thisisasecret!'
db = SQLAlchemy(app)

class students(db.Model):
    ID = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(50))
    gender = db.Column(db.String(10))
    age = db.Column(db.Integer)
    DOB = db.Column(db.Date)
    email = db.Column(db.String(50))

    def __init__(self, ID, name, gender, age, DOB, email):
        self.ID = ID
        self.name = name
        self.gender = gender
        self.age = age
        self.DOB = DOB
        self.email = email

#db.create_all()
#db.session.commit()
#####################################################

# Class Validation
class ContactForm(FlaskForm):
    ID = IntegerField('Your ID', validators=[InputRequired()])
    name = StringField('Your Full Name', validators =[InputRequired()])
    gender = RadioField('Your gender', default='Male', choices=[('Male', 'Male'), ('Female', 'Female'), ('Others', 'Others')])
    age = IntegerField('Your age', validators=[InputRequired()] )
    DOB = DateField('Ex: 1999-11-30', validators=[InputRequired()], format='%Y-%m-%d')
    email = StringField('Email', validators=[InputRequired()])

# ID Validation
class CheckID(FlaskForm):
    ID = IntegerField('Your ID', validators=[InputRequired()])

#Insert
@app.route('/', methods = ['GET', 'POST'])
def student():
    student = ContactForm()
    if student.validate_on_submit():
        try:
            new_st = students(student.ID.data, student.name.data, student.gender.data, student.age.data, student.DOB.data, student.email.data)
            db.session.add(new_st)
            db.session.commit()
            return render_template('result.html', form = student)
        except:
            return '<h1 style="color:red"> This ID is not available  </h1> '
    return render_template('student.html', form = student)

#using ID to delete student
@app.route('/delete', methods = ['GET', 'POST'])
def delete():
    delete = CheckID()
    if delete.validate_on_submit():
        try:
            delete_st = students.query.filter_by(ID = delete.ID.data).first()
            db.session.delete(delete_st)
            db.session.commit()
        except:
            return '<h1 style="color:red"> This ID is not exist </h1>'
    return render_template('find_st.html', form = delete)   

#using ID to show student
@app.route('/show', methods = ['GET', 'POST'])
def show():
    if request.method == "POST":
        details = request.form
        id_show = details['ID']   
        info_st = students.query.filter_by(ID = id_show).first()
        return render_template('show_student.html', info_st = info_st)
    return render_template('show.html')

#show all students
@app.route('/show_all')
def show_all():
    info_st = students.query.all()
    return render_template('show_student.html', info_st = info_st)

#using ID to update information of student
@app.route('/update', methods = ['GET', 'POST'])
def update():
    if request.method == "POST":
        details = request.form
        ID = details['ID']
        name = details['name']
        gender = details['gender']
        age = details['age']
        DOB = details['DOB']
        email = details['email']
        info_st = students.query.filter_by(ID = ID).first()
        print(info_st)
        if name != '':
            info_st.name = name
        if gender != '':
            info_st.gender = gender
        if age != '':
            info_st.age = age
        if DOB != '':
            info_st.DOB = DOB
        if email != '':
            info_st.email = email
        db.session.commit()
        return '<h1> Update Successfully </h1>'
    return render_template('update.html')

if __name__ == '__main__':
    app.run()