from flask_wtf import Form
from wtforms import Form, BooleanField, StringField, SelectField, TextField, IntegerField, SubmitField, validators
from wtforms.validators import InputRequired, Length, DataRequired, Email, NumberRange

#Standard Form Fields & Description
#TextField, BooleanField, DecimalField, IntegerField, RadioField, SelectField, TextAreaField, PasswordField, SubmitField

#Validators Class & Description
#DataRequired, Email, IPAddress, Length, NumberRange, URL

class ContactForm(Form):
    InputRequired
    ID = IntegerField(validators=[InputRequired('ID is required')])
    name = TextField(validators=[DataRequired('Name is required')])
    gender = SelectField(validators=[DataRequired], choices=[('Male', 'Female', 'Other')])
    age = IntegerField(validators=[DataRequired('Age is required'), NumberRange(min = 0, max = 100, message = 'bla bla bla')]) 
    DOB = IntegerField(validators=[DataRequired('DOB is required')])
    email = TextField(validators=[DataRequired(['Email is required']), Email()])